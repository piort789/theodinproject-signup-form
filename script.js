let i = 0;
const SignInForm = document.querySelector(".sign-in-form");

//input error elements

const FirstNameError = document.querySelector(".firstname-error");

const LastNameError = document.querySelector(".lastname-error");

const EmailError = document.querySelector(".email-error");

const PhoneNumberError = document.querySelector(".phone-error");

const PasswordError = document.querySelector(".password-error");

const ConfirmPasswordError = document.querySelector(".confirm-password-error");

// form input elements
const FirstNameInput = document.querySelector("#firstname");

const LastNameInput = document.querySelector("#lastname");

const EmailInput = document.querySelector("#email");

const PhoneNumberInput = document.querySelector("#phone-number");

const PasswordInput = document.querySelector("#password");

const ConfirmPasswordInput = document.querySelector("#confirm-password");

FirstNameInput.addEventListener("input", FirstNameCheckValidity);

LastNameInput.addEventListener("input", LastNameCheckValidity);

EmailInput.addEventListener("input", EmailCheckValidity);

PhoneNumberInput.addEventListener("input", PhoneNumberCheckValidity);

PasswordInput.addEventListener("input", PasswordInputCheckValidity);

ConfirmPasswordInput.addEventListener("input", ConfirmPasswordCheckValidity);

function FirstNameCheckValidity(event) {
  //let isEmailValid=EmailInput.ariaChecked
  if (FirstNameInput.checkValidity()) {
    FirstNameError.innerText = "";
  } else {
    FirstNameError.innerText = "First name is required";
  }
}

function LastNameCheckValidity(event) {
  //let isEmailValid=EmailInput.ariaChecked
  if (LastNameInput.checkValidity()) {
    LastNameError.innerText = "";
  } else {
    LastNameError.innerText = "Last name is required";
  }
}

function EmailCheckValidity(event) {
  if (EmailInput.checkValidity()) {
    EmailError.innerText = "";
  } else {
    EmailError.innerText = "Insert valid email adress";
  }
}

function PhoneNumberCheckValidity(event) {
  //regular expresion for phone number
  let RegExPhone = /^\+?[1-9][0-9]{7,14}$/;
  if (RegExPhone.test(PhoneNumberInput.value)) {
    PhoneNumberError.innerText = "";
  } else {
    PhoneNumberError.innerText = "Insert valid phone number";
  }
}

function PasswordInputCheckValidity(event) {
  let errorMessage = "";
  let RegExCapital = /[A-Z]/g;
  let RegExNumber = /[0-9]/g;
  const password = PasswordInput.value;

  if (password.length < 8) {
    errorMessage += "must be at least 8 characters\n";
  }
  if (!RegExCapital.test(password)) {
    errorMessage += "Must contain capital letter\n";
  }
  if (!RegExNumber.test(password)) {
    errorMessage += "Must contain a number";
  }

  PasswordError.innerText = errorMessage;
  ConfirmPasswordCheckValidity();
}

function ConfirmPasswordCheckValidity(event) {
  if (PasswordInput.value === ConfirmPasswordInput.value) {
    ConfirmPasswordError.innerText = "";
  } else {
    ConfirmPasswordError.innerText = "Paswords do not match";
  }
}
